# Wheel of Fortune - frontend
App for customizable Wheels of Fortune. The goal was to learn typescript along with Spring Boot. Sadly, I had to leave this project unfinished, because of needed preparation for my final exams (maturita).

## Features
- Use of React styled components
- CSS modules
- Next.js way of routing

## Used Technologies
- [Next](https://nextjs.org/) - React framework
- [Yup](https://github.com/jquense/yup) - form validation library
- [Formik](https://formik.org/docs/overview) - React form enhancing library
- [React Font Awesome](https://fontawesome.com/v5.15/how-to-use/on-the-web/using-with/react) - Font Awesome icons for React
