export interface ApiErrorResponse {
    errorMessage: string
    error: string
    timestamp: string
}