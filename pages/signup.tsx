import Head from "next/head"
import { FormType, UserForm } from "../components/UserForm"

const SignUpPage = () => {
    return (
        <>
            <Head><title>Wheel of Fortune | Register</title></Head>
            <div className="center">
                <UserForm type={FormType.SignUp} />
            </div>
        </>
    )
}

export default SignUpPage