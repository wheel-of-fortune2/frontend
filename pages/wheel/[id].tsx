import Head from 'next/head'
import React from 'react'
import Wheel from '../../components/Wheel'

export default function Home() {
    const data = [
        {
            id: 1,
            label: 'Možnost 1',
            probability: .15
        },
        {
            id: 1,
            label: 'Možnost 2',
            probability: .85,
        },
    ]

    return (
        <>
            <Head><title>Wheel of Fortune</title></Head>
            <div className="center">
                <Wheel parts={data}></Wheel>
            </div>
            {/* <WheelCanvas parts={data}></WheelCanvas> */}
        </>
    )
}
