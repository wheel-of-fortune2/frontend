import Head from 'next/head'
import { NewWheelForm, Values } from '../../components/NewWheelForm'
import Wheel from '../../components/Wheel'
import { useState } from 'react'


const New = () => {
    const [wheelValues, setWheelValues] = useState<Values>({
        name: 'New wheel', parts: [
            { label: '', probability: 25 },
            { label: '', probability: 25 },
            { label: '', probability: 25 },
            { label: '', probability: 25 }
        ]
    })

    return (
        <>
            <Head><title>Wheel of Fortune | New wheel</title></Head>
            <div className='two-column-grid'>
                <div className='center'>
                    <NewWheelForm onChange={(values) => setWheelValues(values)} initialValues={wheelValues}></NewWheelForm>
                </div>
                <div className='center bg-grey'>
                    <Wheel parts={wheelValues.parts.map(part => {
                        return {
                            label: part.label,
                            probability: part.probability / 100
                        }
                    })}></Wheel>
                </div>
            </div>
        </>
    )
}

export default New