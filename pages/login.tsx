import Head from "next/head"
import { UserForm, FormType } from "../components/UserForm"

const LoginPage = () => {
    return (
        <>
            <Head><title>Wheel of Fortune | Login</title></Head>
            <div className="center">
                <UserForm type={FormType.Login} />
            </div>
        </>
    )
}

export default LoginPage