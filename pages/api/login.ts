import { NextApiRequest, NextApiResponse } from "next";
import { ApiErrorResponse } from "../../utils/types";

export default async function (req: NextApiRequest, res: NextApiResponse) {
    const apiRes = await fetch(`${process.env.API_URL}/api/users/login`, {
        method: req.method,
        body: req.body,
        headers: {
            'API_KEY': process.env.API_KEY
        }
    })
    const apiResBody: { token: string } | ApiErrorResponse = await apiRes.json()
    res.status(apiRes.status)
    if ('token' in apiResBody)
        sessionStorage.setItem('jwt', apiResBody.token)
    else
        res.json(apiResBody)
}