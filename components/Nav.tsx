import React from 'react'
import styles from '../styles/Nav.module.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars, faTimes, faDharmachakra, faHome } from '@fortawesome/free-solid-svg-icons'
import Link from 'next/link'

const Nav = () => {
    const toggleMenu = () => {
        const nav = document.querySelector(`.${styles.nav}`)
        nav.classList.toggle(styles.active)
    }

    const closeMenu = () => {
        const nav = document.querySelector(`.${styles.nav}`)
        nav.classList.remove(styles.active)
    }

    return (
        <nav className={styles.nav}>
            <ul className={styles.navMenu}>
                <li className={styles.navItem}>
                    <Link href='/'>
                        <a onClick={closeMenu} className={styles.navLink}><FontAwesomeIcon icon={faHome} height={"1.1rem"}></FontAwesomeIcon>&nbsp;Domů</a>
                    </Link>
                </li>
                <li className={styles.navItem}>
                    <Link href='/wheel/my'>
                        <a onClick={closeMenu} className={styles.navLink}><FontAwesomeIcon icon={faDharmachakra} height={"1.1rem"}></FontAwesomeIcon>&nbsp;Moje</a>
                    </Link>
                </li>
                <li className={styles.navItem}>
                    <Link href='/wheel/new'>
                        <a onClick={closeMenu} className={styles.navLink}><FontAwesomeIcon icon={faDharmachakra} height={"1.1rem"}></FontAwesomeIcon>&nbsp;Nové</a>
                    </Link>
                </li>
            </ul>
            <FontAwesomeIcon onClick={toggleMenu} icon={faBars} className={styles.menuIcon}></FontAwesomeIcon>
            <FontAwesomeIcon onClick={toggleMenu} icon={faTimes} className={styles.closeIcon}></FontAwesomeIcon>
        </nav>
    )
}

export default Nav