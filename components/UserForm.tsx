import { Form, Formik } from "formik"
import * as yup from 'yup'
import { SubmitButton } from "./forms/SubmitButton"
import { FormField } from "./forms/FormField"
import Link from "next/link"
import { useRouter } from "next/dist/client/router"
import { useEffect } from "react"
import { ApiErrorResponse } from "../utils/types"

interface Props {
    type: FormType,
}

export enum FormType {
    Login,
    SignUp,
}

interface Credentials {
    username: string
    password: string
}

const validationSchema = yup.object({
    username: yup.string().required('Username is required').max(50),
    password: yup.string().required('Password is required').min(5, 'Password must have at least 5 characters')
})

const login = async (credentials: Credentials) => {
    await fetch('/api/login', {
        method: 'POST',
        body: JSON.stringify(credentials)
    })
}

const signup = async (credentials: Credentials) => {

}

export const UserForm = ({ type }: Props) => {
    const router = useRouter()

    useEffect(() => {
        if (router.query.error) {
            console.log(router.query.error)
        }
    }, [router])

    return (
        <Formik
            initialValues={{ username: '', password: '' }}
            validationSchema={validationSchema}
            onSubmit={(values, { setSubmitting }) => {
                setSubmitting(true)
                switch (type) {
                    case FormType.Login:
                        login(values)
                        break;
                    case FormType.SignUp:
                        signup(values)
                        break;
                }
                console.log(values)
                // onSubmit(values)
                setSubmitting(false)
            }}
        >
            {({ isSubmitting }) => (
                <Form className='form'>
                    <h2 className='text-center'>{type === FormType.Login ? 'Login' : 'Register'}</h2>
                    <FormField
                        label='Username'
                        name='username'
                    />
                    <FormField
                        label='Password'
                        name='password'
                        type='password'
                    />
                    <SubmitButton
                        isSubmitting={isSubmitting}
                        text={type === FormType.Login ? 'Login' : 'Register'}
                        submittingText={type === FormType.Login ? 'Logging In' : 'Saving user'}
                    />
                    <Link href={type === FormType.Login ? '/signup' : '/login'}>
                        <a className='link text-center'>{type === FormType.Login ? 'Sign up' : 'Login'}</a>
                    </Link>
                </Form>
            )}
        </Formik>
    )
}
