import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { ArrayHelpers, ErrorMessage, Field, FormikErrors } from 'formik'
import styles from '../../styles/WheelFormRow.module.scss'
import { Values } from '../NewWheelForm'

interface Props {
    index: number
    values: Values
    arrayHelpers: ArrayHelpers
    onKeyUp: (values: Values, errors: FormikErrors<Values>) => void
    errors: FormikErrors<Values>
}

export const WheelFormRow = ({ index, values, arrayHelpers, errors, onKeyUp }: Props) => {
    const container = `parts.${index}.`
    return (
        <div>
            <div className={styles.inputRow}>
                <div>
                    <label className='label' htmlFor={container + 'label'}>Label</label>
                    <Field className='input' name={container + 'label'} onKeyUp={() => onKeyUp(values, errors)}></Field>
                </div>
                <div>
                    <label className='label' htmlFor={container + 'label'}>Probability</label>
                    <div className={styles.probabilityInput}>
                        <Field className='input' name={container + 'probability'} onKeyUp={() => onKeyUp(values, errors)}></Field>
                    </div>
                </div>
                <button className={`icon-button ${styles.removeRowBtn}`} onClick={() => arrayHelpers.remove(index)}>
                    <FontAwesomeIcon icon={faTimes}></FontAwesomeIcon>
                </button>
            </div>
            <div className={styles.row}>
                <div>
                    <ErrorMessage name={container + 'label'}>{msg => <div className='error'>{msg}</div>}</ErrorMessage>
                </div>
                <div>
                    <ErrorMessage name={container + 'probability'}>{msg => <div className='error'>{msg}</div>}</ErrorMessage>
                </div>
            </div>
        </div>
    )
}