import { ErrorMessage, Field, FieldAttributes } from "formik"

type Props = {
    label: string
} & FieldAttributes<{}>

export const FormField = ({ label, ...props}: Props) => {
    return (
        <div className='form-field'>
            <label className='label' htmlFor={props.name}>{label}</label>
            <Field className='input' {...props}></Field>
            <ErrorMessage name={props.name}>{msg => <div className='error'>{msg}</div>}</ErrorMessage>
        </div>
    )
}