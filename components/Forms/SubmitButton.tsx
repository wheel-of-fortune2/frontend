import { faDharmachakra, IconDefinition } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

interface Props {
    isSubmitting: boolean
    text: string
    submittingText: string
    icon?: IconDefinition
}

export const SubmitButton = ({ isSubmitting, text, submittingText, icon }: Props) => {
    return (
        <button type="submit" disabled={isSubmitting} className={`button form-bottom-btn yellow ${isSubmitting ? 'loader-btn' : ''}`}>
            {isSubmitting
                ? <><FontAwesomeIcon icon={faDharmachakra} /><span>{submittingText}</span></>
                : <>{icon ? <FontAwesomeIcon icon={icon}></FontAwesomeIcon> : ''}{text}</>
            }
        </button>
    )
}