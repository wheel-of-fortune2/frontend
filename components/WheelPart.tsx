import styled from 'styled-components'

type WheelPartProps = {
    readonly wheelRadius: number
    readonly lowestFactor: number
    readonly index: number
    readonly rotation: number
    readonly numberOfParts: number
    readonly angle: number
    readonly partHeight: number
}

const colors = ['#eae2b7', '#fcbf49', '#f77f00', '#d62828']


/**
 * @description
 * Summary of WheelPart generation:
 * 
 * angle = probability * 360
 * 
 * divide angle into 90deg and remainder (144deg -> 90deg + 54deg)
 * 
 * then angles are rendered with 0 width and height and corresponding border-width (max-width of these is 90deg => division to 90deg parts),
 * 
 * centered (because of label position)
 * 
 * rotation of subsequent parts -> half of this part's angle + half of previous part's angle
 */
export default styled.div<WheelPartProps>`
    top: calc(50% - ${props => props.partHeight / 2}px);
    height: ${props => props.partHeight}px;
    width: ${props => props.wheelRadius}px;
    transform: rotate(${props => props.rotation}deg);

    &:before {
        transform: translate(-50%, -50%) rotate(${props => (props.angle % 90) / 2 - Math.abs(Math.floor(props.angle / 90 - 1)) * 45}deg);
        border-width: ${props => props.wheelRadius}px;
        border-color: 
            ${props => props.angle == 360 ? colors[props.index % Math.min(props.lowestFactor, colors.length)] : 'transparent'}
            ${props => props.angle >= 90 ? colors[props.index % Math.min(props.lowestFactor, colors.length)] : 'transparent'}
            ${props => props.angle >= 180 ? colors[props.index % Math.min(props.lowestFactor, colors.length)] : 'transparent'}
            ${props => props.angle >= 270 ? colors[props.index % Math.min(props.lowestFactor, colors.length)] : 'transparent'};
    }

    &:after {
        transform: translateY(-100%) rotate(${props => -(90 - props.angle % 90) / 2 - Math.floor(props.angle / 90 - 1) * 45}deg);
        border-width: 0 0 
            ${props => Math.tan(Math.PI / 180 * (props.angle % 90)) * props.wheelRadius}px 
            ${props => props.wheelRadius}px;
        border-color: transparent transparent ${props => colors[props.index % Math.min(props.lowestFactor, colors.length)]} transparent;
    }

    &:last-child {
        &:after {
            ${props => props.numberOfParts % colors.length == 1 ? 'border-color: transparent transparent red transparent;' : ''}
        }

        &:before {
            ${props => props.numberOfParts % colors.length == 1 ? `border-color:
                ${props.angle == 360 ? 'red' : 'transparent'}
                ${props.angle >= 90 ? 'red' : 'transparent'}
                ${props.angle >= 180 ? 'red' : 'transparent'}
                ${props.angle >= 270 ? 'red' : 'transparent'};` : ''}
        }
    }
`