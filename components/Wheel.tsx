import styles from '../styles/Wheel.module.scss'
import { useEffect, useState } from 'react'
import WheelPart from './WheelPart'
import styled from 'styled-components'

interface Props {
    parts: {
        label: string
        probability: number
    }[]
}

interface WheelProps {
    radius: number
}

const WheelWrapper = styled.div<WheelProps>`
    width: ${props => props.radius}px;
    height: ${props => props.radius}px;
`

const Wheel = ({ parts }: Props) => {
    const [partHeight, setPartHeight] = useState<number>(null)
    const [radius, setRadius] = useState<number>(null)

    useEffect(() => {
        const parentElement = document.querySelector(`.${styles.wheel} `).parentElement
        const radius = Math.min(parentElement.clientHeight - 32, parentElement.clientWidth - 32)
        setRadius(radius)
        setPartHeight([1, 2].includes(parts.length)
            ? radius
            : Math.tan(Math.PI / parts.length) * parentElement.clientHeight)
    }, [])

    const findLowestFactor = (number: number): number => {
        for (let i = 2; i <= Math.sqrt(number); i++) {
            if (number % i === 0) return i
        }
        return number
    }

    let rotation = 0

    return (
        <WheelWrapper radius={radius} className={styles.wheel}>
            {parts.map((part, index) => {
                if (index !== 0)
                    rotation += parts[index - 1].probability * 360 / 2 + part.probability * 360 / 2

                return <WheelPart
                    key={index}
                    className={styles.wheelPart}
                    lowestFactor={findLowestFactor(parts.length)}
                    partHeight={partHeight}
                    index={index}
                    rotation={rotation}
                    angle={360 * part.probability}
                    wheelRadius={radius}
                    numberOfParts={parts.length}>
                    <div className={styles.label} style={{ lineHeight: `${partHeight}px` }}>{part.label}</div>
                </WheelPart>
            })}
        </WheelWrapper>
    )
}

export default Wheel
