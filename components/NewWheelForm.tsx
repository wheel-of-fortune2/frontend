import { Formik, Form, FieldArray, FormikErrors } from 'formik'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus, faSave } from '@fortawesome/free-solid-svg-icons'
import * as yup from 'yup'
import styles from '../styles/NewWheelForm.module.scss'
import { SubmitButton } from './forms/SubmitButton'
import { WheelFormRow } from './forms/WheelFormRow'
import { FormField } from './forms/FormField'

export interface Values {
    name: string
    parts: {
        label: string
        probability: number
    }[]
}

interface Props {
    initialValues?: Values
    onChange: (values: Values) => void
}

const validationSchema = yup.object({
    name: yup.string().required('Name is required').max(50),
    parts: yup.array().of(yup.object({
        label: yup.string().max(20, 'Too long'),
        probability: yup.string().required('Probability is required')
            .notOneOf(['0'], 'Probability can\'t be 0')
            .matches(/(^100(\.0{1,2})?$)|(^([1-9]([0-9])?|0)(\.[0-9]{1,2})?$)/g, 'Invalid format')
    })).min(1, 'Wheel must consist of at least one part').test('sum', 'Sum of probabilities must be 100', (parts) => {
        const probabilities = parts.map(part => (+part.probability ?? 0))
        const total = probabilities.reduce((total, probability) => total + (probability || 0), 0)
        return total === 100
    })
})

const onSubmit = (values: Values) => {

}

export const NewWheelForm = ({ initialValues, onChange }: Props) => {
    const onKeyUp = (values: Values, errors: FormikErrors<Values>) => {
        if (Object.keys(errors).length === 0) onChange(values)
    }

    return (
        <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={(values, { setSubmitting }) => {
                setSubmitting(true)
                console.log(values)
                // onSubmit(values)
                setSubmitting(false)
            }}
        >
            {({ isSubmitting, values, errors }) => (
                <Form className='form'>
                    <h2 className='text-center'>New Wheel</h2>
                    <FormField 
                        label='Name'
                        name='name'
                        onKeyUp={() => onKeyUp(values, errors)}
                    />
                    <FieldArray name='parts'>
                        {(arrayHelpers) =>
                            <>
                                <div className={styles.rowsWrapper}>
                                    {values.parts.map((part, index) =>
                                        <WheelFormRow
                                            key={index}
                                            index={index}
                                            values={values}
                                            arrayHelpers={arrayHelpers}
                                            errors={errors}
                                            onKeyUp={onKeyUp}
                                        />
                                    )}
                                </div>
                                <div className='error'>{typeof errors.parts === 'string' ? errors.parts : null}</div>
                                <div className={styles.buttonWrapper}>
                                    <button className='button form-bottom-btn grey' onClick={() => {
                                        const probabilityTotal = values.parts.reduce((total, part) => total + +part.probability, 0)
                                        return arrayHelpers.push({
                                            label: '',
                                            probability: Math.max(0, 100 - probabilityTotal)
                                        })
                                    }}><FontAwesomeIcon icon={faPlus}></FontAwesomeIcon>Add part</button>
                                    <SubmitButton
                                        isSubmitting={isSubmitting}
                                        text='Save'
                                        submittingText='Saving'
                                        icon={faSave}
                                    />
                                </div>
                            </>
                        }
                    </FieldArray>
                </Form>
            )
            }
        </Formik >
    )
}